File importer
=============

A Symfony project created on October 27, 2016, 18:15 pm.

Usage:

* Clone the project

* Go to project root folder

* Run 

```
#!bash

$ composer install
```

* Databases settings is in file parameters.yml

* Create database 
```
#!bash

$ bin/console doctrine:database:create

```
* Create tables
```
#!bash

$ bin/console doc:mig:mig

```
* CSV default location ***/client_data/accountinfo.csv***

* Run the import:


```
#!bash

$ bin/console import:account-info

```

* Run tests:

```
#!bash

$ phpunit
```