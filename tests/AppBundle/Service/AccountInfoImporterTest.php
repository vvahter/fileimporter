<?php

namespace Tests\AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Service\AccountInfoImporter;

class AccountInfoImporterTest extends \PHPUnit_Framework_TestCase
{
    private $importer;

    public function setUp()
    {
        $em = $this->getMockEntityManager();
        $em->method('isOpen')->willReturn(true);
        $this->importer = new AccountInfoImporter($em);
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage File 'foo' does not exist!
     */
    public function testFileDoesNotExists()
    {
        $this->importer->import('foo', true);
    }

    public function testReadExistingFile()
    {
        $expectedLog = [
            "Row 2 not valid: Row must have 3 values: Transaction ID, Transaction Date, Amount",
            "Row 3 not valid: '2016/02/30 17:17:36 +0100' is not a valid date",
            "Row 4 not valid: '5999a' is not a valid amount",
            "Row 5 not valid: Date missing",
            "Row 6 not valid: Transaction ID missing",
            "Row 7 not valid: Amount",
            "Import completed! Success: 6, Fail: 6",
        ];

        $this->assertEquals($expectedLog, $this->importer->import($this->getTestFileName(), true));
    }

    private function getTestFileName()
    {
        return sprintf("%s/tests/AppBundle/Service/AccountInfoTestFile.csv", getcwd());
    }

    private function getMockEntityManager()
    {
        return $this
            ->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();
    }
}
