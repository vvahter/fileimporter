<?php

namespace AppBundle\Service;

use AppBundle\Entity\AccountInfo;
use AppBundle\Entity\AccountInfoLog;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Exception\InvalidArgumentException;

class AccountInfoImporter
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Import account info.
     *
     * @param string $filePath
     * @param boolean $skipFirst
     *
     * @return array
     */
    public function import($filePath, $skipFirst)
    {
        $log = [];

        if (!$this->currencyFileExists($filePath)) {
            throw new InvalidArgumentException(sprintf("File '%s' does not exist!",
                $filePath));
        }

        $accountsInfo = $this->readAccountantInfoCsv($filePath);
        $rowNr = 0;

        foreach ($accountsInfo as $accountInfo) {
            $rowNr++;

            // Skip first row if needed
            if ($skipFirst) {
                $skipFirst = false;
                continue;
            }

            try {
                // Validate csv row
                $this->validateCsvRow($accountInfo);

                // Create new Account Info
                $accountInfo = new AccountInfo(
                    $accountInfo[0],
                    new \DateTime($accountInfo[1]),
                    $accountInfo[2]);

                // Save account info to database
                $this->em->persist($accountInfo);
                $this->em->flush();
            } catch (\Exception $e) {
                $this->resetEm();
                $log[] = sprintf("Row %s not valid: %s",
                    $rowNr, $e->getMessage());
            }
        }

        $logErrorCount = count($log);
        $log[] = sprintf("Import completed! Success: %s, Fail: %s", $rowNr - $logErrorCount, $logErrorCount);

        $this->writeLog($log);

        return $log;
    }

    /**
     * Write logs into database.
     *
     * @param array $log
     */
    private function writeLog(array $log)
    {
        $accountInfoLog = new AccountInfolog(new \DateTime(), json_encode($log));

        $this->em->persist($accountInfoLog);
        $this->em->flush();
    }

    /**
     * When entity manager is closed due to exception then reopen it to continue the import.
     */
    private function resetEm()
    {
        if (!$this->em->isOpen()) {
            $this->em = $this->em->create(
                $this->em->getConnection(),
                $this->em->getConfiguration()
            );
        }
    }

    /**
     * Validate csv row.
     *
     * @param array $row
     *
     * @return null
     */
    private function validateCsvRow(array $row)
    {
        if (3 != count($row)) {
            throw new \Exception("Row must have 3 values: Transaction ID, Transaction Date, Amount");
        }

        if (!$row[0]) {
            throw new \Exception("Transaction ID missing");
        }

        if (!$row[1]) {
            throw new \Exception("Date missing");
        }

        if (!$row[2]) {
            throw new \Exception("Amount");
        }

        $date = new \DateTime($row[1]);
        $errors = $date->getLastErrors();

        if ($errors['warning_count'] != 0 || $errors['error_count'] != 0) {
            throw new \Exception(sprintf("'%s' is not a valid date", $row[1]));
        }

        if (!is_numeric($row[2])) {
            throw new \Exception(sprintf("'%s' is not a valid amount", $row[2]));
        }

    }

    /**
     * Read file.
     *
     * @param String $filePath
     *
     * @return Array
     */
    private function readAccountantInfoCsv($filePath)
    {
         return array_map('str_getcsv', file($filePath));
    }

    /**
     * Check if file exist.
     *
     * @param string $fileName
     *
     * @return bool
     */
    private function currencyFileExists($filePath)
    {
        $fs = new Filesystem();

        return $fs->exists($filePath);
    }

}