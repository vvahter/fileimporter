<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AccountInfoImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
        // the name of the command (the part after "bin/console")
        ->setName('import:account-info')

        // the short description shown while running "php bin/console list"
        ->setDescription('Import account info from csv.')

        // the full command description shown when running the command with
        // the "--help" option
        ->setHelp("This command allows you to import account info from csv to table accounting_data")

        // --skipFirst=1 - skip csv first line
        ->addOption(
            'skipFirst',
            null,
            InputOption::VALUE_REQUIRED,
            'Do we skip the file header row? (value 0 or 1)',
            1
        )

        // --filepath=foo/bar.csv - the csv location
        ->addOption(
            'filepath',
            null,
            InputOption::VALUE_REQUIRED,
            'Csv file path',
            "/client_data/accountinfo.csv"
        );
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $skipFirst = !!$input->getOption('skipFirst');
        $filepath = $input->getOption('filepath');
        $today = new \DateTime();

        $output->writeln([
            '<info>Account info importer</info>',
            sprintf('<info>%s</info>', $today->format('Y-m-d H:i:s')),
            '<info>============</info>',
            '',
        ]);

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $log = $em->getRepository('AppBundle:AccountInfoLog')->findTodaysLog($today);

        if ($log) {
            throw new \Exception("The file is already imported!");
        }

        $output->writeln(sprintf('Skip first line: %s', $skipFirst ? 'yes' : 'no'));
        $output->writeln(sprintf('CSV location: %s', $filepath));

        $importer = $this->getContainer()->get('account_info_importer');
        $log = $importer->import($filepath, $skipFirst);


        // Write log to screen
        $output->writeln($log);

        $output->writeln([
            '',
            '<info>============</info>',
            '<info>Account info import ended</info>',
            '',
        ]);
    }
}