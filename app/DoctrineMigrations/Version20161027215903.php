<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161027215903 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE accounting_data (id INT AUTO_INCREMENT NOT NULL, transaction_id VARCHAR(255) NOT NULL, date DATETIME NOT NULL, amount DOUBLE PRECISION NOT NULL, created_at DATETIME NOT NULL, UNIQUE INDEX transaction_date_amount (transaction_id, date, amount), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

        $this->addSql('CREATE TABLE account_info_log (id INT AUTO_INCREMENT NOT NULL, date DATETIME NOT NULL, comment LONGTEXT NOT NULL, UNIQUE INDEX UNIQ_2F1E130FAA9E377A (date), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE account_info');
        $this->addSql('DROP TABLE account_info_log');
    }
}
